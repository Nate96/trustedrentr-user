package com.trustedrentr.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO - Exception Handling
 * 			* create a Exception class 
 * 			* put exception messages in application.properties
 * TODO - Logging
 */

@SpringBootApplication
public class UserApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}
}

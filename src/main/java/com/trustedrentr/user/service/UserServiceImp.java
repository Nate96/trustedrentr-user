package com.trustedrentr.user.service;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

import com.trustedrentr.user.dto.UserDTO;
import com.trustedrentr.user.entity.User;
import com.trustedrentr.user.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO addUser(UserDTO userDTO) throws IllegalArgumentException {
        User user = createUser(userDTO);
        userRepository.save(user);
        return User.toDTO(user);
    }

    @Override
    public Boolean removeUser(Integer userId) throws IllegalArgumentException {
        userRepository.deleteById(userId);
        return true;
    }

    @Override
    public UserDTO updateUser(UserDTO userDTO) {
        User user = userRepository.findById(userDTO.getUserID()).get();

        user.setUpdatedAt(LocalDateTime.now());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setRentScore(userDTO.getRentScore());
        user.setUserRole(userDTO.getUserRole());

        userRepository.save(user);
        
        return User.toDTO(user);
    }

    @Override
    public UserDTO getUserById(Integer userId) throws NoSuchElementException {
        return User.toDTO(userRepository.findById(userId).get());
    }

    private User createUser(UserDTO userDTO) {
        User user = new User();

        user.setCreatedAt(LocalDateTime.now());
        user.setUpdatedAt(LocalDateTime.now());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setRentScore(userDTO.getRentScore());
        user.setUserRole(userDTO.getUserRole());

        return user;
    }
    
}

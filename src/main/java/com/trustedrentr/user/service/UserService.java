package com.trustedrentr.user.service;

import java.util.NoSuchElementException;

import com.trustedrentr.user.dto.UserDTO;

public interface UserService {
    public UserDTO addUser(UserDTO userDTO); 
    public Boolean removeUser(Integer userId);
    public UserDTO updateUser(UserDTO userDTO);
    public UserDTO getUserById(Integer userId) throws NoSuchElementException;
}
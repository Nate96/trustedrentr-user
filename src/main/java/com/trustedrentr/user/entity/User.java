package com.trustedrentr.user.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

import com.trustedrentr.user.dto.UserDTO;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Integer rentScore;
    private String firstName;
    private String lastName;
    private String userRole;
    private String displayName;
    private String email;
    private boolean emailVerified; 
    private Integer phoneNumber;

    public void setEmailVerified(boolean emailVerified) { this.emailVerified = emailVerified; }

    public boolean getEmailVerified() { return this.emailVerified; }

    public void setPhoneNumber(Integer phoneNumber) { this.phoneNumber = phoneNumber; }

    public Integer getPhoneNumber() { return this.phoneNumber; }

    public void setEmail(String email) { this.email = email; }

    public String getEmail() { return this.email; }

    public void setDisplayName(String displayName) { this.displayName = displayName; }

    public String getDisplayName() { return this.displayName; }

    public Integer getUserID() { return this.userId; }

    public void setUserID(Integer userId) { this.userId = userId; }

    public LocalDateTime getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(LocalDateTime createdAt) { this.createdAt = createdAt; }

    public LocalDateTime getUpdatedAt() { return this.updatedAt; }

    public void setUpdatedAt(LocalDateTime updatedAt) { this.updatedAt = updatedAt; }

    public Integer getRentScore() { return this.rentScore; }

    public void setRentScore(Integer rentScore) { this.rentScore = rentScore; }

    public String getFirstName() { return this.firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return this.lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getUserRole() { return this.userRole; }

    public void setUserRole(String userRole) { this.userRole = userRole; }

    public static UserDTO toDTO(User user) { 
        UserDTO userDTO = new UserDTO();

        userDTO.setCreatedAt(user.getCreatedAt());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setRentScore(user.getRentScore());
        userDTO.setUpdatedAt(user.getUpdatedAt());
        userDTO.setUserID(user.getUserID());
        userDTO.setUserRole(user.getUserRole());
        userDTO.setEmail(user.getEmail());
        userDTO.setEmail(user.getEmail());
        userDTO.setEmailVerified(user.getEmailVerified());
        userDTO.setPhoneNumber(user.getPhoneNumber());
        userDTO.setDisplayName(user.getDisplayName());
        

        return userDTO;
    }
 
}

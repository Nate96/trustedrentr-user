package com.trustedrentr.user.api;

import com.trustedrentr.user.dto.UserDTO;
import com.trustedrentr.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "user")
public class UserAPI {

     @Autowired
     private UserService userService;

     @GetMapping(value = "/{userId}")
     public ResponseEntity<UserDTO> getUser(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<UserDTO>(userService.getUserById(userId), HttpStatus.OK);
     }

     @PostMapping
     public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<UserDTO>(userService.addUser(userDTO), HttpStatus.OK);
     }

     @PutMapping
     public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO) { 
         return new ResponseEntity<UserDTO>(userService.updateUser(userDTO), HttpStatus.OK);
     }

     @DeleteMapping(value = "/{userId}")
     public ResponseEntity<Boolean> deleteUser(@PathVariable("userId") Integer userId) {
        userService.removeUser(userId);  
        return new ResponseEntity<Boolean>(true, HttpStatus.OK); 
     }
    
}

// Use ResponseEntity because it is easier to control headers and status codes.
// https://restfulapi.net/http-methods/
// https://docs.oracle.com/en/cloud/saas/enterprise-performance-management-common/prest/rest_api_methods.html
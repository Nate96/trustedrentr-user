package com.trustedrentr.user.repository;

import com.trustedrentr.user.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> { }
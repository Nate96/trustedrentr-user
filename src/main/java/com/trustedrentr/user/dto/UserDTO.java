package com.trustedrentr.user.dto;

import java.time.LocalDateTime;

import com.trustedrentr.user.entity.User;

public class UserDTO {
    private Integer userId;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Integer rentScore;    
    private String firstName;
    private String lastName;
    private String userRole;
    private String displayName;
    private String email;
    private boolean emailVerified; 
    private Integer phoneNumber;

    public void setEmailVerified(boolean emailVerified) { this.emailVerified = emailVerified; }

    public void updateEmailVerified() { this.emailVerified = !this.emailVerified; }

    public boolean getEmailVerified() { return this.emailVerified; }

    public void setPhoneNumber(Integer phoneNumber) { this.phoneNumber = phoneNumber; }

    public Integer getPhoneNumber() { return this.phoneNumber; }

    public void setEmail(String email) { this.email = email; }

    public String getEmail() { return this.email; }

    public void setDisplayName(String displayName) { this.displayName = displayName; }

    public String getDisplayName() { return this.displayName; }

    public Integer getUserID() { return this.userId; }

    public void setUserID(Integer userId) { this.userId = userId; }

    public LocalDateTime getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(LocalDateTime createdAt) { this.createdAt = createdAt; }

    public LocalDateTime getUpdatedAt() { return this.updatedAt; }

    public void setUpdatedAt(LocalDateTime updatedAt) { this.updatedAt = updatedAt; }

    public Integer getRentScore() { return this.rentScore; }

    public void setRentScore(Integer rentScore) { this.rentScore = rentScore; }

    public String getFirstName() { return this.firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return this.lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getUserRole() { return this.userRole; }

    public void setUserRole(String userRole) { this.userRole = userRole; }

    public static User toEntity(UserDTO userDTO) { 
        User user = new User();

        user.setUserID(userDTO.getUserID());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setCreatedAt(userDTO.getCreatedAt());
        user.setUpdatedAt(userDTO.getUpdatedAt());
        user.setRentScore(userDTO.getRentScore());
        user.setUserRole(userDTO.getUserRole());
        user.setEmail(userDTO.getEmail());
        user.setEmailVerified(userDTO.getEmailVerified());
        user.setDisplayName(userDTO.getDisplayName());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        return user;
    }

}
